JFLAGS = -g
JC = javac
JVM = java
CC = -classpath ".:sqlite-jdbc-3.14.2.1.jar"
.SUFFIXES: .java .class
.java.class:
	$(JC) $(JFLAGS) $*.java

CLASSES = \
	TAX.java \
	Goods.java \
	FoodOrMed.java \
	GoodsTax.java

MAIN = TAX

default: classes

run: $(MAIN).class 
	$(JVM) $(CC) $(MAIN)

classes: $(CLASSES:.java=.class)

clean:
	$(RM) *.class
