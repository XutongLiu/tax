
public class FoodOrMed extends Goods
{

	public FoodOrMed(int number, double price, String name, boolean isImported){
		super(number, price, name, isImported);
	}

	public double calculateTax()
	{
		double p = this.price;
		if( this.getIsImported() == true)
		{
			return (this.number * (p * 0.05));
		}
		return 0;
	}
	
	public double calculateTotal()
	{
		return this.price * this.number + this.calculateTax();
	}
}
