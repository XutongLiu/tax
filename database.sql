drop table goods;

create table goods (
  number int not null, 
  price decimal(18,2),
  name varchar(20),
  FoM boolean,
  Imp boolean
);


--insert into goods values (1, 27.99, "imported bottle of perfume", 0, 1);
--insert into goods values (1, 18.99, "bottle of perfume", 0 ,0);
--insert into goods values (1, 9.75, "packet of headache pills", 1,0);
--insert into goods values (1, 11.25, "imported box of chocolates", 1,1);

--insert into goods values (1, 10.00, "imported box of chocolates", 1,1);
--insert into goods values (1, 47.50, "imported bottle of perfume", 0,1);

insert into goods values (10, 27.99, "imported bottle of perfume", 0, 1);
insert into goods values (10, 18.99, "bottle of perfume", 0 ,0);
insert into goods values (10, 9.75, "packet of headache pills", 1,0);
insert into goods values (10, 11.25, "imported box of chocolates", 1,1);
