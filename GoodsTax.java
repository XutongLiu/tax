
public interface GoodsTax 
{
	public double calculateTax();
	public double calculateTotal();
}
