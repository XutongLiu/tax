
public class Goods implements GoodsTax
{
	int number;
    double price;
    boolean isImported;
	String name;
    
	public Goods(int number, double d, String name, boolean isImported) 
	{
		super();
		this.number = number;
		this.price = d;
		this.name = name;
		this.isImported = isImported;
	}

	public int getNumber() 
	{
		return number;
	}

	public void setNumber(int number) 
	{
		this.number = number;
	}

	public double getPrice()
	{
		return price;
	}

	public void setPrice(int price)
	{
		this.price = price;
	}

	public String getName() 
	{
		return name;
	}

	public void setName(String name) 
	{
		this.name = name;
	}
	
	public boolean getIsImported()
	{
		return this.isImported;
	}
	
	public double calculateTax()
	{
		double p = this.price;
		if( this.isImported == true)
		{
		   	return this.number * 0.15 * p;
		}
		return this.number * p * 0.1;
		
	}
	
	public double calculateTotal()
	{
		return this.price * this.number + this.calculateTax();
	}
}

