import java.util.*;
import java.text.DecimalFormat;
import java.math.*;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
public class TAX 
{

	public static void main(String[] args) 
	{
        Connection conn = null;
        try
        {
        	Class.forName("org.sqlite.JDBC");
        	conn = DriverManager.getConnection("jdbc:sqlite:good.db");
            Statement stmt = conn.createStatement();
            ResultSet set = stmt.executeQuery("SELECT * from goods;");

            List<Goods> list = new ArrayList<>();

            while( set.next())
            {
                int number = set.getInt("number");
                double price = set.getDouble("price");
                String name = set.getString("name");
                Boolean isFoodOrMilk = set.getBoolean("FoM");
                Boolean isImported = set.getBoolean("Imp");

                if( isFoodOrMilk == true)
                {
                	Goods good = new FoodOrMed(number, price, name, isImported);
                	list.add(good);
                }
                else
                {
                	Goods good = new Goods(number, price, name, isImported);
                	list.add(good);
                }

            }

            DecimalFormat df = new DecimalFormat("#.00");
            df.setRoundingMode(RoundingMode.HALF_UP); 
		    if( list.size() != 0)
		    {
                double tax = 0;
	            double total = 0;
		        for( int i = 0; i < list.size(); i++)
		        {
			        Goods temp = list.get(i);
			        tax += temp.calculateTax();
			        total += temp.calculateTotal();
			        String s = temp.getNumber() + " "+ temp.getName() + ": " + df.format(temp.calculateTotal());
			        System.out.println(s);
		        }
		        System.out.println("TAX: " + df.format(tax));
		        System.out.println("TOTAL: " + df.format(total));
		    }
        
        }
        catch(Exception e)
        {
        	throw new RuntimeException("There was a runtime problem!", e);
        }
        finally 
        {
            try 
            {
                if (conn != null) conn.close();
            } 
            catch (SQLException e) 
            {
                throw new RuntimeException(
                    "Cannot close the connection!", e);
            }
        }
		
	}
	
}
