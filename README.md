#Code Assignment, TAX

------

Assumption I made:

1. Input, it is a list of item. I think it will be a list of item objects. So,
   I made a database to contains all the details and convert them to a list of
   object with JDBC.

2. Output, print out onto the commmand line

-----

When test this assignment:

1. edit database.sql, for modify data

2. run 
   $sqlite good.db < database.sql 
   at command line

3. type 
   $make run 
   at command line

----

In this assignment, I use factory pattern. So, if any more types of goods has 
their own rules for tax, just create a child of goods. Also, a good can be
imported or not imported. So, I create imported as a boolean.

----

Where to check the code:

git clone git@bitbucket.org:XutongLiu/tax.git 

Or

git clone https://XutongLiu@bitbucket.org/XutongLiu/tax.git

Or
   
https://1drv.ms/f/s!Akznj3_4KG8ThPYgf1Q0QUo1sTrBEg  (oneDrive)

Also, my jdbc is sqlite-jdbc-3.14.2.1.jar. If something does not work well, 
may be it is. 

If the code gets something wrong. Email me as soon as possible.

